import numpy as np
import sys
import ipdb
import matplotlib.pyplot as plt

if __name__ == "__main__":
    if len(sys.argv) <4:
        print ("Usage:\n python plot_txtfile.py"+
             " /path/to/file \"delimiter\" column0 column3 column4 etc \n"+
             "Example: \n"+
             "python plot_txtfile.py /Users/me/data.txt \",\" 1 3 4")
        sys.exit(0)
    filename = sys.argv[1]
    delim = sys.argv[2].replace("\"", "")
    columns = [int(x) for x in sys.argv[3:]]
    ipdb.set_trace()
    lines= np.genfromtxt(filename, dtype=str, delimiter=delim, skiprows = 1)

    xvals = np.array(lines[:,columns[0]], dtype=float)
    if len(columns) == 1:
        plt.plot(xvals, linestyle='--', marker='o')
        plt.show()
    else:
        yvals = [np.array(lines[:, j], dtype=float) for j in columns[1:]]

        for i in range(len(yvals)):
            plt.plot(xvals, yvals[i], linestyle='--', marker='o' )
        plt.title(filename + "\ncolumns: "+
                ('[%s]' % ', '.join(map(str, columns))))             
        plt.show()
