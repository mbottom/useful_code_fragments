import os
import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
import gc

for i in range(100):
    a=np.arange(10000)
    b=a**2*(1+np.random.random(10000)*0.1)
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.plot(a,b)
    fig.savefig('img'+str(i).zfill(4)+'.png', format='png')
    fig.clf()
    plt.close()
    gc.collect()

if not os.path.exists(os.path.join(os.getcwd(),'animation')):
    path=os.path.join(os.getcwd(),'animation')
    os.system('mkdir '+path)

path=os.path.join(os.getcwd(),'animation')
os.system("mv *.png "+path)

fps=str(10)
os.system("cd "+path+" && "+"ffmpeg -r "+fps+" -i img%04d.png -c:v libx264 -r 30 -pix_fmt yuv420p output.mp4 -y && rm *.png")

