import os

#finds all pdf files in a directory

rootdir='/Users/Me/Dropbox/fellowship_proposals'

files=[] 
for dirpath,_,filenames in os.walk(rootdir):
    for f in filenames:
        if f.endswith('.pdf'):
            files.append(os.path.abspath(os.path.join(dirpath, f)))

print files
