import matplotlib.pyplot as plt
import numpy as np
import time

plt.ion()
for i in range(10):
    plt.plot(np.random.random(10), np.random.random(10))
    plt.draw()
    plt.pause(0.01)
    time.sleep(1)
    plt.clf()
