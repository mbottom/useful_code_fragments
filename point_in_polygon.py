import pdb
import numpy as np
import matplotlib.pyplot as plt

def rootsofunity(n):
    #roots of unity in clockwise order
    return np.array(
           [np.exp(1j*2*np.pi*i/n) for i in range(n)])

def polyvertices(n, rad=1.0, rotangle= None):
    if rotangle == None:
        rotangle = 1j*np.pi/n
    rou = rootsofunity(n)
    rot_rou = rou*np.exp(1j*rotangle)
    return  zip(np.real(rot_rou)*rad,
               np.imag(rot_rou)*rad)

def point_in_poly(x,y,poly):

    n = len(poly)
    inside = False

    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x,p1y = p2x,p2y

    return inside

def points_in_poly(xvals, yvals, polyvertices):
    #use meshgrid before
    ans=np.zeros(np.shape(xvals))
    for i in range(np.shape(xvals)[0]):
        for j in range(np.shape(xvals)[1]):
            if point_in_poly(xvals[i, j], yvals[i,j], polyvertices):
                ans[i,j] = 1
    return ans

def points_in_poly_decenter(xvals, yvals, xcen, ycen, polyvertices):
    ans=np.zeros(np.shape(xvals))
    for i in range(np.shape(xvals)[0]):
        for j in range(np.shape(xvals)[1]):
            if point_in_poly((xvals[i, j]-xcen), 
                             (yvals[i,j]-ycen), polyvertices):
                ans[i,j] = 1
    return ans
## Test

polygon=polyvertices(6, 1 )


points_x = np.linspace(-2, 2, 1024)
points_y = np.linspace(-2, 2, 1024)

xvals, yvals= np.meshgrid(points_x, points_y)

## Call the function with the points and the polygon
#print point_in_poly(points_x,points_y,polygon)
plt.contourf(xvals, yvals, points_in_poly_decenter(xvals, yvals, 0.5, 0.5, polygon))
plt.axis('equal')
plt.show()
