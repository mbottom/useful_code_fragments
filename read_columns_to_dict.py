import numpy as np
import pprint
import ipdb
#THIS ASSUMES A 1 LINE HEADER AT THE TOP

def dictfromfile(filename, delim=','):
    ipdb.set_trace()
    lines= np.genfromtxt(filename, dtype=str, delimiter=delim, skiprows = 0)

    dicty = {}
    for i in range(len(lines[0,:])):
        try:
            dicty[str(lines[0,i]).strip(" ")]=np.array(
                lines[1:, i], dtype=float)
        except:
            dicty[str(lines[0,i]).strip(" ")]=np.array(
                lines[1:, i], dtype=str)
    return dicty

if __name__ == "__main__":
    """Usage: dict = dictfromfile("myfile.txt", delim=" ")"""
    filename = 'filelist.txt'
    dicty = dictfromfile(filename)
    pprint.pprint(dicty)
        
